'use strict'

import { app, BrowserWindow ,ipcMain} from 'electron'
var electron = require('electron')

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  console.log('CodeFirst公司制作!')
  /**
   * Initial window options
   */
  var winW = electron.screen.getPrimaryDisplay().workAreaSize.width
  var winH = electron.screen.getPrimaryDisplay().workAreaSize.height
  mainWindow = new BrowserWindow({
    // height: winH + 40,
    useContentSize: true,
    frame: false,
    show: false,
    fullscreen: false,
    // width: winW
    width:1366,
    height:768,
    minWidth:1366,
    minHeight:768,
    movable: true,
    resizable:true
  })
  mainWindow.on('resize',()=>{
    if (!mainWindow.isMaximized()){
      BrowserWindow.getFocusedWindow().webContents.send('restoreMaximize','restore');
    }else {
      BrowserWindow.getFocusedWindow().webContents.send('restoreMaximize','maximize');
    }
  });
  mainWindow.loadURL(winURL)
  // if (!mainWindow.isFullScreen()) {
  //   mainWindow.setFullScreen(true)
  // }
  mainWindow.on('closed', () => {
    mainWindow = null
  })

  mainWindow.on('synchronous-message', (event, arg) => {
    debugger
    if (arg === 'logined') {
      mainWindow.resize(1366, 768)
    }
  })
  // 关闭窗体
  mainWindow.on('closeWindow', () => {
    mainWindow.close()
  })
  mainWindow.on('asynchronous-message', (event, arg) => {
    console.log('sb')
  })
  mainWindow.show()
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})


ipcMain.on('min', e=> mainWindow.minimize());
ipcMain.on('max', e=> mainWindow.maximize());
ipcMain.on('close', e=> mainWindow.close());

ipcMain.on('window-small',(event,arg)=>{
  mainWindow.restore();
  mainWindow.setFullScreen(false);
  mainWindow.setSize(1366,768)
  console.log('变小')
});
ipcMain.on('window-max',(event,arg)=>{
  var winW = electron.screen.getPrimaryDisplay().workAreaSize.width
  var winH = electron.screen.getPrimaryDisplay().workAreaSize.height
  mainWindow.setSize(winW,winH+40)
  console.log('最大化')
});
/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
